from pymongo import MongoClient
from models.items import database as db
import csv, json
from bson import ObjectId

db = db()

def objIdToStr(obj):
    return str(obj["_id"])

def search_item_by_name(**params):
    data_list = []
    for item in db.searchItemByName(**params):
        item["_id"] = objIdToStr(item)
        data_list.append(item)
    return data_list

def search_items():
    data_list = []
    for item in db.showItems():
        item["_id"] = objIdToStr(item)
        data_list.append(item)
    return data_list

def search_items_id(**params):
    result = db.showItemById(**params)
    result["_id"] = objIdToStr(result)
    return result

def insert_item(**params):
    result = db.insertItem(**params)
    result["_id"] = objIdToStr(result)
    return result

def update_item(**params):
    result = db.updateItemById(**params)
    result["_id"] = objIdToStr(result)
    return result

def delete_item(**params):
    result = db.deleteItemById(**params)
    result["_id"] = objIdToStr(result)
    return result