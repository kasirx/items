Aplikasi Kasir

Ini adalah aplikasi kasir yang memiliki dua fitur/dua project yaitu Customer dan Items.
di project ini fiturnya adalah Items, terdapat fungsi :

1. Menambahkan Data
2. Membaca Data
3. Mengubah Data
4. Menghapus Data

Keterangan database :
Nama database = kasir
Nama tabel = items
host="localhost"
port=27017

Di project ini menggunakan database Mongodb, FastAPI,pymongo
Daftar Rest Api

1. /items = Melihat semua data Items
2. /itembyid = Melihat data item berdasarkan id
3. /itembyname = Melihat data item berdasarkan nama
4. /insert = untuk menambahkan data item baru
5. /update = untuk merubah data item
6. /delete = untuk menghapus data item
