from fastapi import FastAPI
from items_route import router as items_router

app = FastAPI()

app.include_router(items_router)

@app.get("/")
async def read_main():
    return {"message" : "Hello Bigger Applications"}

