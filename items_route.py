from fastapi import APIRouter
from items_api import *

router = APIRouter()

@router.post("/itembyid")
async def view_search_items_id(params:dict):
    result = search_items_id( **params)
    return result

@router.post("/itembyname")
async def view_search_items_name(params:dict):
    result = search_item_by_name( **params)
    return result

@router.post("/items")
async def view_search_items():
    result = search_items()
    return result

@router.post("/insert")
async def view_insert_item(params:dict):
    result = insert_item( **params)
    return result

@router.post("/update")
async def view_update_item(params:dict):
    result = update_item( **params)
    return result

@router.post("/delete")
async def view_delete_item(params:dict):
    result = delete_item( **params)
    return result