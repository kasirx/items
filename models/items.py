from pymongo import MongoClient
from bson.objectid import ObjectId

class database:
    def __init__ (self):
        try:
            self.nosql_db =MongoClient(host="localhost",port=27017)
            self.db = self.nosql_db.kasir
            self.mongo_col = self.db.items
            print("database connected")
        except Exception as e:
            print(e)

    def showItems(self):
        result = self.mongo_col.find()
        return [item for item in result]
    
    def showItemById(self,**params):
        result = self.mongo_col.find_one({"_id":ObjectId(params["id"])})
        return result
    
    def searchItemByName(self, **params):
        query = {"namaBarang" : {"$regex": "{0}".format(params["namaBarang"]), "$options": "i"}}
        result = self.mongo_col.find(query)
        return result
    
    def insertItem(self,document):
        self.mongo_col.insert_one(document)
    
    def updateItemById(self,**params):
        query_1 = {"_id":ObjectId(params["id"])}
        query_2 = {"$set": params["data"]}
        result = self.mongo_col.update(query_1,query_2)
        return result
    
    def deleteItemById(self, **params):
        query = {"_id":ObjectId(params["id"])}
        result = self.mongo_col.remove(query)
        return result